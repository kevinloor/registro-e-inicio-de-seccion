const registerForm = document.getElementById("register-form");
registerForm.addEventListener("submit", function(event) {
	event.preventDefault();
	const username = document.getElementById("username").value;
	const email = document.getElementById("email").value;
	const password = document.getElementById("password").value;
	// Aquí puedes utilizar una petición HTTP para enviar los datos a un servidor y guardarlos en una base de datos
});

const loginForm = document.getElementById("login-form");
loginForm.addEventListener("submit", function(event) {
	event.preventDefault();
	const username = document.getElementById("login-username").value;
	const password = document.getElementById("login-password").value;
	// Aquí puedes utilizar una petición HTTP para verificar los datos de inicio de sesión en un servidor y permitir el acceso al usuario
});
